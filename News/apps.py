from django.apps import AppConfig # pragma: no cover


class NewsConfig(AppConfig): # pragma: no cover
    name = 'News'
