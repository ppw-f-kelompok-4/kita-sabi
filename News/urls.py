from django.urls import path
from .views import showNews

app_name = 'News'
urlpatterns = [
 path('', showNews, name='news')
]