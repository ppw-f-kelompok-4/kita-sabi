from django.test import TestCase, Client
from .models import News

#Create your tests here.
class Test(TestCase):
	def test_news_exist(self):
		response = Client().get('/News')
		self.assertEqual(response.status_code,200)
		self.assertTemplateUsed(response, "news.html")
		
	def test_model_exist(self):
		News.objects.create(title="banjir", image="https://marvel-live.freetls.fastly.net/canvas/2018/10/a157d44e6d9e460a96fa2886e34d5182?quality=95&fake=.png", date="2018-10-1", author="shania", source = "detik", description = "abcdef")
		total = News.objects.all().count()
		self.assertEqual(total,1)