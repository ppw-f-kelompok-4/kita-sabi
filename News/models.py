from django.db import models

# Create your models here.
class News(models.Model):
	title = models.CharField(max_length=30)
	image = models.URLField()
	date = models.DateTimeField()
	author = models.CharField(max_length=30)
	source = models.CharField(max_length=30)
	description = models.TextField()