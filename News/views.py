from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import News

# Create your views here.
response = {}
def showNews(request):
	response['news'] = News.objects.all()
	return render(request, 'news.html', response)