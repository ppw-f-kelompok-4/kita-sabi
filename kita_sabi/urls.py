"""kita_sabi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import RedirectView
from django.contrib.auth import views
from django.conf import settings
from django.conf.urls.static import static
import Homepage.urls as Homepage
import News.urls as News
import Register.urls as Register
import Donate.urls as Donate
import About.urls as About
import daftardonasi.urls as daftardonasi
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='Home')),
    path('Home', include(Homepage), name="Home"),
    path('News', include(News)),
    path('Register', include(Register)),
    path('Donate/', include(Donate)),
    path('About/', include(About)),
    path('daftardonasi/', include(daftardonasi)),
] +static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
