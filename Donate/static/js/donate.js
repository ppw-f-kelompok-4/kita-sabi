var donasi_is_valid = false;

function validasiInput() {
    if ($('#program').val() && $('#donasi').val() && donasi_is_valid) {
        $('button[type=submit]').removeAttr('disabled');
    } else {
        $('button[type=submit]').attr('disabled', 'true');
    }
}

$(document).ready(function () {
    $('#donasi').after("<div id='invalid_amount' style='display: none' class='alert alert-danger alert-dismissible fade show' role='alert'></div>");
    $('button[type=submit]').after("<div id='failed' style='display: none' class='alert alert-danger' role='alert'></div>");
    $('button[type=submit]').after("<div id='success' style='display: none' class='alert alert-success' role='alert'></div>");
    $(document).on('change', '#program', '#donasi', function () {
        validasiInput();
    });
    
    $('#donasi').on('change', function () {
        var donasi = $(this).val()
        $.ajax({
            method: 'POST',
            type: 'POST',
            url: 'cek-donasi/',
            data: {
                'program': $('#program').val(),
                'jumlah-donasi' : donasi,
            },
            dataType: 'json',
            success: function (result) {
                var buttonClose = "<button type='button' " +
                                      "class='close' data-dismiss='alert' aria-label='Close'>" +
                                      "<span aria-hidden='true'>&times;</span></button>"
                if (result.isValid) {
                    $('#invalid_amount').css('display', 'none');
                    $('#donasi').css('border-color', '#ced4da');
                    donasi_is_valid = result.isValid
                } else {
                    $('#invalid_amount').html(result.message + buttonClose)
                    $('#invalid_amount').css('display', 'block')
                    $('#donasi').css('border-color', 'red')
                    donasi_is_valid = result.isValid
                }
                validasiInput();
            }
        });
    });

    $('#form-donation').on('submit', function () {
        var program = $('#program');
        var jumlah = $('#donasi');
        var anon = $('#anon');
        var data = {
            'program': program.val(),
            'jumlah': jumlah.val(),
            'anonymous': anon.is(':checked'),
        };
        $.ajax({
            method: 'POST',
            url: 'submit-donation/',
            dataType: 'json',
            data: data,
            success: function (result) {
                if (result.success) {
                    $('#success').html(result.message);
                    $('#success').css('display', 'block');
                    $('#failed').css('display', 'none');
                    $('#valid_email').css('display', 'none');
                    $('#invalid_email').css('display', 'none');
                    jumlah.val("");
                    program.val("");
                } else {
                    $('#failed').html(result.message);
                    $('#failed').css('display', 'block');
                    $('#success').css('display', 'none');
                    $('#valid_email').css('display', 'none');
                    $('#invalid_email').css('display', 'none');
                }
            }
        });
        event.preventDefault();
    });
});