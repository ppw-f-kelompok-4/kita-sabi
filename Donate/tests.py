from importlib import import_module

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .views import showDonationForm, cekDonasi
from .models import Donasi
from Homepage.models import DisastersModels
from Register.models import reg

# Create your tests here.

class FormDonasiUnitTest(TestCase):

    def test_redirected_if_not_logged_in(self):
        response = Client().get('/Donate')
        self.assertEqual(response.status_code, 301)

    def test_donation_form_is_exist(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        response = c.get('/Donate/')
        self.assertEqual(response.status_code, 200)

    def test_donation_form_using_showDonationForm_func(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        response = resolve('/Donate/')
        self.assertEquals(response.func, showDonationForm)

    def test_model_can_create_new_donation(self):
        Donasi.objects.create(
            program=DisastersModels.objects.create(DisasterName='Donasi Palu'),
            nama='Muhamad Lutfi Arif',
            jumlah='900000',
            anonymous=True
        )
        count_all_donation = Donasi.objects.all().count()
        self.assertEqual(count_all_donation, 1)

    def test_get_form_function(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        response = c.get('/Donate/submit-donation/')
        self.assertEqual(response.status_code, 200)

    def test_donation_success(self):
        user = User.objects.create_user(username='testuser', email='test@gmail.com')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345', email='test@gmail.com')
        DisastersModels.objects.create(DisasterName='Donasi Palestina', SumOfDonation=0)
        program = 1
        jumlah = 1000
        response_post = c.post('/Donate/submit-donation/', {
            'program' : program,
            'jumlah' : jumlah,
        })
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        cekDonasi(request)
        self.assertEqual(response_post.status_code, 200)

    def test_donation_fail_of_invalid_amount(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        DisastersModels.objects.create(DisasterName='Donasi Aceh', SumOfDonation=0)
        program = 1
        jumlah = -1
        response_post = c.post('/Donate/submit-donation/', {
            'program': program,
            'jumlah': jumlah,
        })
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        cekDonasi(request)
        self.assertEqual(response_post.status_code, 200)



