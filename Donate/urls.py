from django.urls import path
from .views import showDonationForm, submitDonation, cekDonasi

app_name = 'Donate'
urlpatterns = [
    path('', showDonationForm, name='donasi'),
    path('submit-donation/', submitDonation, name='submit-donation'),
    path('cek-donasi/', cekDonasi, name='cek-donasi'),
]