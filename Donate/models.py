from django.db import models
from Homepage.models import DisastersModels
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

class Donasi(models.Model):
    program = models.ForeignKey(DisastersModels, on_delete=models.CASCADE, null=True)
    nama = models.CharField(max_length=30, null=True)
    email = models.CharField(max_length=30, null=True)
    jumlah = models.IntegerField(null=True, blank=True,
                                 validators=[
                                     MaxValueValidator(100000000),
                                     MinValueValidator(0)
                                 ])
    anonymous = models.BooleanField(default=True)

    def __str__(self):
        return self.nama


