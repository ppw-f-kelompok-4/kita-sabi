from django.db import IntegrityError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .forms import FormDonasi
from .models import Donasi
from Homepage.models import DisastersModels, Donatur

import json
# Create your views here.

def showDonationForm(request):
    if request.user.is_authenticated:
        donasi = Donasi.objects.all()
        form = FormDonasi()
        html = 'donate.html'
        nama = request.user.username
        return render(request, html, {'donasi': donasi, 'form': form, 'name': nama})
    else:
        return HttpResponseRedirect('/Register')

@csrf_exempt
def cekDonasi(request):
    response = {
        'message': '',
        'isValid': False
    }
    form = FormDonasi(data={
        'jumlah': request.POST.get('jumlah-donasi'),
        'program': request.POST.get('program')
    })
    if form.is_valid() == False:
        response['message'] = 'Jumlah uang yang didonasikan tidak valid'
    else:
        response['isValid'] = True
    json_data = json.dumps(response)
    return HttpResponse(json_data)

@csrf_exempt
def submitDonation(request):
    response={}
    try:
        if request.method == 'POST':
            if request.POST.get('anonymous') == 'true':
                nama = request.user.username
            else:
                nama = 'Hamba Allah'
            email = request.user.email
            id_program = request.POST.get('program')
            program = DisastersModels.objects.get(id=id_program)
            jumlah = request.POST.get('jumlah')
            donatur = Donatur.objects.create(nama=nama, jumlah=jumlah)
            program.SumOfDonation += int(jumlah)
            program.donaturs.add(donatur)
            valid_input = Donasi(program=program,
                                 nama=nama,
                                 email=email,
                                 jumlah=jumlah)
            valid_input.save()
            program.save()
            response['success'] = True
            response['message'] = 'Berhasil! Terimakasih, ' + request.user.username + '. Donasi sudah Anda berhasil!'
            json_data = json.dumps(response)
            return HttpResponse(json_data)
        else:
            response['success'] = False
            response['message'] = 'Donasi gagal!'
            json_data = json.dumps(response)
            return HttpResponse(json_data)
    except IntegrityError as e:
        response['success'] = False
        response['message'] = 'Integrity Error'
        json_data = json.dumps(response)
        return HttpResponse(json_data)
