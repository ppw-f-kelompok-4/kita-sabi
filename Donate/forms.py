from django import forms

from .models import Donasi


class FormDonasi(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(FormDonasi, self).__init__(*args, **kwargs)

        self.fields['program'].label = ''
        self.fields['program'].widget.attrs = {'id': 'program', 'class': 'form-control', 'placeholder': 'Nama Program'}

        self.fields['jumlah'].label = ''
        self.fields['jumlah'].widget.attrs = {'id': 'donasi', 'class': 'form-control', 'placeholder': 'Jumlah uang'}

        self.fields['anonymous'].label = 'Tampilkan nama Anda dalam list donatur'
        self.fields['anonymous'].widget.attrs = {'id': 'anon'}
        self.fields['anonymous'].initial = True
    class Meta:
        model = Donasi
        fields = ['program', 'jumlah', 'anonymous']
