from django.apps import AppConfig # pragma: no cover


class DonateConfig(AppConfig): # pragma: no cover
    name = 'Donate'
