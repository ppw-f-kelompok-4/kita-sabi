from django.urls import path
from .views import about, create_testimoni

app_name = 'About'
urlpatterns = [
 path('', about, name='about'),
 path('create-testimoni/', create_testimoni, name='create-testimoni'),
]