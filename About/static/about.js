$(document).ready(function() {
	$(".testimoni").on('submit', function(){
		event.preventDefault();

		var csrftoken = $("[name=csrfmiddlewaretoken]").val();
		var payload = $(".testimoni-form-textarea").val(); 
		console.log(payload);
		var data = {
            'testimoni': payload
        };

		$.ajax({
			method : "POST",
			url : "create-testimoni/",
			headers:{
		        "X-CSRFToken": csrftoken
		    },
		    dataType: 'json',
			data : data,
			success: function(data) {
				var newTesti = '<div class="card text-white bg-success mb-3" style="max-width: 18rem;">';
				newTesti += '<div class="card-header">' + data.nama + '</div>';
				newTesti += '<div class="card-body">';
				newTesti += '<p class="card-text"> ' +  data.testimoni + ' </p></div></div>';
				$("#id_testimoni").val('');
				console.log(data);
				// $("#new-testimoni").append(newTesti).fadeIn();
				$(newTesti).hide().appendTo("#new-testimoni").fadeIn(1000);
				$(".testimoni").val("");
			}
		});
	});
});