from django import forms

class testimoniForm(forms.Form):
	error_messages = {
		'required': 'This field is required',
	}
	attrs = {
		'class': 'testimoni-form-textarea',
		'type': 'text',
		'rows': 6,
		'cols' : 115,
		'resize' : 'none',
	}
    
	testimoni = forms.CharField(label='Testimoni', required=True, max_length=500, widget=forms.Textarea(attrs=attrs))