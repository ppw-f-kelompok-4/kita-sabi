from django.shortcuts import render
from django.http import HttpResponse
from .forms import testimoniForm
from .models import testimoniModels
from django.views.decorators.csrf import csrf_exempt

import json
# Create your views here.
def about(request):
	result = testimoniModels.objects.all()
	login = "false"
	if request.user.is_authenticated:
		login = "true"
	form = testimoniForm()
	return render(request, 'about.html', {'form' : form, 'testimoni':result, 'login': login})

@csrf_exempt
def create_testimoni(request):
	response={}
	if request.method == 'POST':
		response['nama'] = request.session['name']
		testimoni = request.POST.get('testimoni')
		createTestimoni = testimoniModels.objects.create(nama=response['nama'], testimoni=testimoni) 
		response['testimoni'] = testimoni
		json_data = json.dumps(response)
		return HttpResponse(json_data)
	else:
		response['success'] = False
		response['message'] = 'Error'
		json_data = json.dumps(response)
		return HttpResponse(json_data)