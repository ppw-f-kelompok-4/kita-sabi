from django.test import TestCase, Client
from .models import testimoniModels

# Create your tests here.
class Test(TestCase):
	def test_url_exist(self):
		response = Client().get('/About/')
		self.assertEqual(response.status_code,200)
		self.assertTemplateUsed(response, "about.html")
		
	def test_model_exist(self):
		testimoniModels.objects.create(testimoni="hehe")
		total = testimoniModels.objects.all().count()
		self.assertEqual(total,1)