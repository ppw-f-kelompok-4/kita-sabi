from django.apps import AppConfig # pragma: no cover


class RegisterConfig(AppConfig): # pragma: no cover
    name = 'Register'
