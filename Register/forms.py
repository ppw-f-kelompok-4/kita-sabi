from django import forms

class regForm(forms.Form):
    attrsName = {
        'class' : 'form-control',
        'placeholder' : 'Nama Lengkap'
    }
    
    attrsBirthdate = {
        'class' : 'form-control',
        'type' : 'date',
        'placeholder' : 'Tanggal Lahir'
    }
    
    attrsEmail = {
        'class' : 'form-control',
        'placeholder' : 'Email'
    }
    
    attrsPassword = {
        'class' : 'form-control',
        'placeholder' : 'Sandi'
    }
    
    name = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrsName), required = True, label = "")
    birthdate = forms.DateField(widget=forms.DateInput(attrs=attrsBirthdate), required = True, label = "")
    email = forms.EmailField(max_length = 30, widget=forms.EmailInput(attrs=attrsEmail), required = True, label = "")
    password = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs=attrsPassword), required = True, label = "")