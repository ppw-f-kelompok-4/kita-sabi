from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
import google
from google.oauth2 import id_token
from google.auth.transport import requests as request2
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import logout as lout
from django.contrib.auth import login as lin

from .forms import regForm
from .models import reg

response = {'author' : 'Bagus Pribadi'}
def showRegisterForm(request):
    regs = reg.objects.all()
    form = regForm(request.POST)
    context = {
        'form' : form,
    }
    return render(request, 'register.html', context)
    
def submitRegistration(request):
    if request.method == 'POST':
        form = regForm(request.POST)
        response['name'] = request.POST['name']
        response['birthdate'] = request.POST['birthdate']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        re = reg(name = response['name'], birthdate = response['birthdate'], email = response['email'], password = response['password'])
        re.save()
        form = regForm()
        return HttpResponseRedirect(reverse('Register:register'))
    else:
        form = regForm()
        regs = reg.objects.all()
        context = {
            'form': form,
            'regs': regs
        }
        return render(request, 'register.html', context)
        
def login(request):
    if (request.method == "POST"):
        response = {
            'status' : "0"
        }
        try:
            toVerify = request.POST['token']
            info = id_token.verify_oauth2_token(toVerify, request2.Request(),"706201221112-pm1bbjno6mqki3d8tu6pv1gi1aepmf1c.apps.googleusercontent.com")
            if (info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']):
                raise ValueError('Wrong issuer.')
            return JsonResponse(response)
        except ValueError:
            response["status"] = "1"
    return JsonResponse({'error': request.method})
    
def logout(request):
    request.session.flush()
    lout(request)
    return HttpResponseRedirect(reverse('Register:register'))
    
def yourProfile(request):
    if request.method == "POST":
        try:
            userid = request.POST['id']
            email = request.POST['emailUrl']
            name = request.POST['name']
            request.session['user_id'] = userid
            request.session['emailUrl'] = email
            request.session['name'] = name
        except ValueError:
            return JsonResponse({"status": "1"})
    if not (User.objects.filter(email=request.POST['emailUrl']).exists()):
        user = User.objects.create_user(username=request.POST["name"], email=request.POST["emailUrl"])
        user.save()
        lin(request, user)
    else:
        user = User.objects.get(email=request.POST['emailUrl'])
        user.save()
        lin(request, user)
    return HttpResponseRedirect(reverse('Homepage:index'))