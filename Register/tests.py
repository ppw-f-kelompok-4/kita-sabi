from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth import get_user_model
from .views import showRegisterForm
from .forms import regForm
from .models import reg
import unittest

class TestPage(TestCase):

    def test_register_exist(self):
        response = Client().get('/Register')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, "register.html")
        
    def test_form_title(self):
        request = HttpRequest()
        resp = showRegisterForm(request)
        html_resp = resp.content.decode('utf8')
        self.assertIn('registrasi untuk donasi', html_resp)
        
    def test_button(self):
        request = HttpRequest()
        resp = showRegisterForm(request)
        html_resp = resp.content.decode('utf8')
        self.assertIn('Daftar', html_resp)

class TestModels(TestCase):
    def test_model_exist(self):
        reg.objects.create(name="JKT 48", birthdate="1999-02-19", email="JKT48@gmail.com", password="oyoyoyOYOY")
        amount = reg.objects.all().count()
        self.assertEqual(amount,1)
    
class TestForms(TestCase):
    def test_form_available(self):
        form = regForm()
        self.assertIn("name", form.as_p())
        self.assertIn("birthdate", form.as_p())
        self.assertIn("email", form.as_p())
        self.assertIn("password", form.as_p())

    def test_form_empty_input(self):
        data = { 'email' : '' }
        form = regForm(data)
        self.assertFalse(form.is_valid())
   
class TestViews(TestCase):
    def test_form_to_model(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['name'] = "ViewTest"
        request.POST['birthdate'] = "2000-01-02"
        request.POST['email'] = "blabla@yahoo.com"
        request.POST['password'] = "kucinghitam13"
        showRegisterForm(request)