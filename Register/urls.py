from django.urls import path
from .views import showRegisterForm, submitRegistration, login, logout, yourProfile

app_name = 'Register'
urlpatterns = [
    path('', showRegisterForm, name='register'),
    path('submit-registration', submitRegistration, name='submit-registration'),
    path('logout', logout, name='logout'),
    path('login', login, name='login'),
    path('yourProfile', yourProfile, name='yourProfile'),
]