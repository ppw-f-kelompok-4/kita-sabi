from django.db import models
from django.utils import timezone

class reg(models.Model):
    name = models.CharField(max_length = 30)
    birthdate = models.DateField(max_length = 30, default = timezone.now)
    email = models.EmailField(max_length = 30)
    password = models.CharField(max_length = 30)