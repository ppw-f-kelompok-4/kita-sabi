from django.contrib import admin

from .models import DisastersModels, Donatur

# Register your models here.
admin.site.register(Donatur)
admin.site.register(DisastersModels)