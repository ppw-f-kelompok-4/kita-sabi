from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .views import index

# Create your tests here.

class HomepageUnitTest(TestCase):
    def test_homepage_is_exist(self):
        response = Client().get('/Home')
        self.assertEquals(response.status_code, 200)

    def test_homepage_using_index_func(self):
        response = resolve('/Home')
        self.assertEquals(response.func, index)

