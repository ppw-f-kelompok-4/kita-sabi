from django.db import models

# Create your models here.


class Donatur(models.Model):
    nama = models.CharField(max_length=30)
    jumlah = models.IntegerField()


    def __str__(self):
        return self.nama

class DisastersModels(models.Model):
    HIGHLIGHT = 'Highlight'
    CARD = 'Card'
    HIGHLIGHT_CARD_CHOICES = (
        (HIGHLIGHT, 'Highlight'),
        (CARD, 'Card'),
    )
    DisasterImage = models.URLField(max_length = 500)
    DisasterName = models.CharField(max_length = 30)
    DisasterDesc = models.TextField(max_length= 150)
    HighlightOrCard = models.CharField(max_length = 10, choices = HIGHLIGHT_CARD_CHOICES)
    donaturs = models.ManyToManyField(Donatur, blank=True)
    SumOfDonation = models.IntegerField(blank=True, null=True)


    def __str__(self):
        return self.DisasterName

    class Meta:
        ordering=['-HighlightOrCard']



    



    

