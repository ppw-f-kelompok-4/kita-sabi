from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from .models import DisastersModels

# Create your views here.

def index(request):
    HighlightDisaster = DisastersModels.objects.filter(HighlightOrCard = "Highlight")
    CardDisaster = DisastersModels.objects.filter(HighlightOrCard = "Card")
    FirstHighlight = DisastersModels.objects.filter(HighlightOrCard = "Highlight").first()
    response = {"HighlightDisaster" : HighlightDisaster, "CardDisaster" : CardDisaster, "FirstHighlight" : FirstHighlight}
    return render(request, 'index.html', response)

    
