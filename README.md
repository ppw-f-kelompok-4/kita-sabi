# KITA SABI - Crowd Funding
[![coverage report](https://gitlab.com/ppw-f-kelompok-4/kita-sabi/badges/master/coverage.svg)](https://gitlab.com/ppw-f-kelompok-4/kita-sabi/commits/master)
[![pipeline status](https://gitlab.com/ppw-f-kelompok-4/kita-sabi/badges/master/pipeline.svg)](https://gitlab.com/ppw-f-kelompok-4/kita-sabi/commits/master)

# Kelompok 4 Kelas PPW F

1706043853 - Ariq Haryo Setiaki

1706043941 - Bagus Pribadi

1706979360 - Muhamad Lutfi Arif

1706044175 - Shania Nabilah

# Link Herokuapp

[http://kita-sabi.herokuapp.com/](http://kita-sabi.herokuapp.com/)

