from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from Donate.models import Donasi
from django.http import JsonResponse
import json
from Donate.models import Donasi

# Create your views here.

def DaftarDonasi(request):
    return render(request, 'daftardonasi.html')

# @login_required
def ListDonasi(request):
    if request.method == "GET":
        # name = request.session["userGivenName"]
        # data = list(Donasi.objects.filter(nama=name).values())
        email = request.user.email
        ListDonasi = Donasi.objects.filter(email=email)
        data = []
        for donasi in ListDonasi:
                data.append({})
                dictionary = data[-1]
                dictionary["namaProgram"] = donasi.program.DisasterName
                dictionary["jumlah"] = donasi.jumlah
        # Menunggu authentifikasinya dapat digunakan, akan menunggu
        
        return JsonResponse(data, safe=False)

