from django.test import TestCase, Client
from django.urls import resolve
import unittest
from daftardonasi.views import *
# Create your tests here.

class DaftarDonasiUnitTest(TestCase):
    def test_url_daftardonasi_exist(self):
        response = Client().get('/daftardonasi/')
        self.assertEqual(response.status_code, 200)

    def test_views_using_DaftarDonasi_func(self):
        found = resolve('/daftardonasi/')
        self.assertEqual(found.func, DaftarDonasi)

