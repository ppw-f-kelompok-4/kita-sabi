from django.urls import path
from daftardonasi.views import *

app_name = "daftardonasi"
urlpatterns = [
    path('', DaftarDonasi, name='daftardonasi'),
    path('ListDonasi/', ListDonasi, name='ListDonasi'),
]

